const { Schema, model } = require('mongoose');

const userSchema = new Schema({  
  username: {
    type: String,
    required: true,
    min: 6
  },
  password: {
    type: String,
    required: true,
    min: 6
  },
  balance: {
    type: Number,
    default: 0
  }
})

module.exports = model('users', userSchema);
