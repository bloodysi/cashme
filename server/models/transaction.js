const { Schema, model } = require('mongoose');

const transactionSchema = new Schema({
  subject: {
    type: String,
    required: true,
    min: 4
  },
  money: {
    type: Number,
    required: true,
  },
  type: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: new Date(),
    required: true,
  },
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  }
})

module.exports = model('transaction', transactionSchema)