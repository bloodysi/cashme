const Strategy = require('passport-local').Strategy;
const User = require('../models/users');
const { compareSync } = require('bcryptjs')

const loginStrategy = new Strategy(
  async (username, password, done) => {

  //Check user
  const user = await User.findOne({ username })
  if(!user){
    done('Username or password invalid', false)
  }

  //Compare password
  const isValid = compareSync(password, user.password)
  if(!isValid){
    done('Username or password invalid', false)
  }

  done(null, user)

})

module.exports = loginStrategy;
