const passport = require('passport')
const User = require('../models/users');

//Serialize & Deserialize

passport.serializeUser((user, done)=> {
  done(null, user.username)
})

passport.deserializeUser(async (id, done)=> {
  const user = await User.findOne({ username: id })
  if(user) return done(null, user)
  done(null, false)
})

//Requires strategeries
const loginStrategy = require('./loginStrategy')
const signupStrategy = require('./signupStrategy')


//Use strategy

passport.use('login', loginStrategy);
passport.use('signup', signupStrategy);




module.exports = passport;