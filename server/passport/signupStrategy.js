const Strategy = require('passport-local').Strategy;
const User = require('../models/users');
const { hashSync } = require('bcryptjs')

const signupStrategy = new Strategy({
  passReqToCallback: true
}, async (req, username, password, done) => {
  
  // Check if user exist
  
  const user = await User.findOne({ username: username })
  if(user){
    done('username already exist', false)
  }

  //Hash password

  const encryptedPassword = hashSync(password, 10)

  //Creating user

  const newUser = new User({
    username,
    password: encryptedPassword,
  })

  try {

    //Saving user

    const boby = await newUser.save()
    if(boby){
      done(null, boby, { message: 'successfull' })
    }
    
  }catch(err){
    done(err)
  }

})

module.exports = signupStrategy;
