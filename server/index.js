const express = require('express');
const cookieSession = require('cookie-session');

const passport = require('./passport');
const userRoute = require('./routes/users');
const transactionRoute = require('./routes/transactions');

const app = express()

//Settings
const { server } = require('./config');

//DB
require('./db')

//Middleawares
app.use(express.json())
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2']
}))
app.use(passport.initialize())
app.use(passport.session())

//Routes
app.use('/users', userRoute)
app.use('/action', transactionRoute)

//Initialization
app.listen(server.port, ()=> console.log('server on!', server.port))