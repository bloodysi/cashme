const router = require('express').Router();
const Transaction = require('../models/transaction');
const User = require('../models/users');

//Post transaction

router.post('/transaction', async(req, res)=> {
  const { subject, money, type, userId } = req.body;
  console.log(req.user)
  try {
    //creating transaction
    const newTransaction = new Transaction({
      subject,
      money,
      type,
      userId
    })
    //saving transaction
    const transaction = await newTransaction.save()
    //update user balance
    const user = await User.findById(req.user.id)
    if(type === 'Income'){
      user.balance += money;      
    }
    else if(type === 'Expense'){
      user.balance -= money
    }
    //saving user
    await user.save()
    
    res.status(201).json({error: false, body: transaction})
  }
  catch(e){
    res.status(500).json({errorr: true, body: e.message})
  }
})

//Post Balance

router.post('/balance/:id', async (req, res)=> {
  try {
    const user = await User.findById(req.params.id)
    user.balance += req.body.balance
    await user.save()
    res.status(200).json({error: false, body: user})
  } catch (e) {
    res.status(500).json({error: true, body: e.message})
  }
})

// Get transaction

router.get('/transaction/:id', async (req, res)=> {
  try {
    const transactions = await Transaction.find({userId: req.params.id})    
    res.status(200).json({error: false, body: transactions})
  } catch (e) {
    res.status(500).json({error: true, body: e.message})
  }
})

module.exports = router;