const router = require('express').Router();
const passport = require('../passport');
const User = require('../models/users');

//register
router.post('/register', (req, res, next)=> {

  passport.authenticate('signup', (err, user, info)=> {
    if(err) res.status(400).json({
      error: true,
      body: err
    })
    if(user){
      req.logIn(user, err => {
        if(err) res.status(400).json({
          error: true,
          body: err
        })
        res.status(200).json(user)
      })
    }
  })(req, res, next)

})

//login
router.post('/login', (req, res, next)=> {
  console.log('post login')
  passport.authenticate('login', (err, user, info)=> {
    if(err) res.status(200).json({
      error: true,
      body: err
    })
    if(user){
      req.logIn(user, err => {
        if(err) res.status(400).json({
          error: true,
          body: "not login"
        })
        res.status(200).json(user)
      })
    }
  })(req, res, next)

})

//Logout

router.get('/logout', (req, res)=> {
  console.log(req.user)
  req.logOut()
  res.status(200).send('success logout')
})

//GET

router.get('/user/:id', async (req, res)=> {
  try {
    const user = await User.findById(req.params.id)
    res.status(200).json({error: false, body: user})
  } catch (e) {
    res.status(200).json({error: true, body: e.message})
  }
})

module.exports = router;
