const mongoose = require('mongoose');
const { database } = require('./config');

mongoose.connect(database.uri, {useNewUrlParser: true, useUnifiedTopology: true}, (err)=> {
  if(err) console.error(err)
  else console.log('db is conected')
})

module.exports = mongoose;
