module.exports = {
  server: {
    port: process.env.PORT || 5000
  },
  database: {
    uri: process.env.DB_URI || 'mongodb://localhost/money-track'
  }
}