import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './app.css';

import NavBar from './components/navbar/NavBar';
import Home from './components/home/Home';
import Footer from './components/footer/Footer';
import Login from './components/login/Login';
import Register from './components/register/Register';
import RegisterBalance from './components/registerBalance/RegisterBalance';

function App() {
  return (
    <div className="App">
      <Router>
        <NavBar />
          <div className='cont'>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route exact path='/login' component={Login} />
              <Route exact path='/register' component={Register} />
              <Route exact path='/register-balance' component={RegisterBalance} />
            </Switch>
          </div>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
