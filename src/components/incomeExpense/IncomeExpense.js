import React, { useContext } from 'react';
import './incomeExpense.css';

import { Context } from '../../context';

const IncomeExpense = (props) => {

  const { items } = useContext(Context)

  const income = items.filter(item => item.type === 'Income')
  const expense = items.filter(item => item.type === 'Expense')

  const incomeTotal = income.map(item => item.money)
  const expenseTotal = expense.map(item => item.money)

  return (
    <div className='expense-real-container'>
      <p className='expense-title'>{props.title}</p>
      <div className={props.title === 'Income' ? 'expense-container green' : 'expense-container red'}>      
        <h3 className='expense-money'>
          ${props.title === 'Income'
          ? incomeTotal.reduce((a,c)=> a + c, 0) 
          : expenseTotal.reduce((a,c)=> a + c, 0)
          }
        </h3>
      </div>
    </div >
  )
}

export default IncomeExpense;
