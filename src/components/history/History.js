import React, { useContext } from 'react';
import './history.css';

import { Context } from '../../context';

const History = ({ setAddingBalance }) => {

  const { items } = useContext(Context)

  // const date = new Date()

  const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

  // const month = date.getMonth()
  // const day = date.getDate()
  
  let date = {
    year: '',
    month: '',
    day: ''
  };

  const handleDate = (year, month, day) => {
    date.year = year
    date.month = months[month]
    date.day = day
  }

  handleDate(200, 5, 5)

  return (
    <div>

      <p className='title-section center'>HISTORY <button onClick={()=> setAddingBalance(true)} className='add-btn' id='addButton'><i class="fas fa-plus"></i></button></p>
      <div className='history-items'>
        <h5 className='history-date'>{date.month + ' ' + date.day}</h5>
        <ul>            
          {(items.length > 0)
            ?
            items.map(item => (              
              <div key={item._id}>
                {handleDate(item.date.slice(0, 4), item.date.slice(6, 7), item.date.slice(8, 10) )}
                <li className={item.type === 'Income' ? 'history-item-container green' : 'history-item-container red'}>
                  <p className='history-item-subject'>{item.subject}</p>
                  <p className='history-item-money'>${item.money}</p>
                  <button className='delete-btn'><i className="fas fa-ellipsis-h"></i></button>
                </li> 
              </div>
            ))
            :
            <h2 className='history-empty'>empty</h2>
          }     
        </ul>
      </div>

    </div>
  )
}

export default History;
