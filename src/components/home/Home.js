import React, { useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import './home.css';

import { Context } from '../../context';

import Balance from '../balance/Balance';
import IncomeExpense from '../incomeExpense/IncomeExpense';
import History from '../history/History';
import AddTransaction from '../addTransaction/AddTransaction';

const Home = () => {

  const [addingBalance, setAddingBalance] = useState(false)

  const { user } = useContext(Context)

  if(!user){
    return <Redirect to='/login' />
  }
  if(user.balance === 0) {
    return <Redirect to='register-balance' />
  }

  return (
    <div className='home-container'>      
      <div className='home-balance'>
        <Balance />
        <div className='income-container'>
          <IncomeExpense title='Income'/>
          <IncomeExpense title='Expense'/>
        </div>
      </div>
      <div className='home-history'>
        { addingBalance ? <AddTransaction setAddingBalance={setAddingBalance} />  : <History setAddingBalance={setAddingBalance} /> }
      </div>
      {
        addingBalance 
        ?
        <div className='ghost'>
          <AddTransaction />
        </div>
        :
        <div className='transaction-container'>
          <AddTransaction setAddingBalance={setAddingBalance}/>
        </div>
      }
    </div>
  )
}

export default Home;
