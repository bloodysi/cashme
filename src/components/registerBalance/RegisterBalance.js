import React, { useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import './registerBalance.css';

import { Context } from '../../context';

const RegisterBalance = () => {

  const [balance, setBalance] = useState('')

  const { user, handleBalance } = useContext(Context)

  if(!user){
    return <Redirect to='/login' />
  }
  if(user.balance > 0){
    return <Redirect to='/' />
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    handleBalance(balance)
  }

  return (
    <div className='wrapper middle center column'>
      <h1 className='welcome'>Hello, {user.username}</h1>
      <h2>Insert your</h2>
      <form onSubmit={(e)=> handleSubmit(e) }>
          <div className='balance-input'>
            <input 
              type='number' 
              className='center f-40' 
              placeholder='Current money' 
              value={balance}
              onChange={(e)=> setBalance(e.target.value)}
            />
            <button type='submit' className={balance > 0 ? 'round-btn' : 'none'}><i className="fas fa-arrow-right"></i></button>
          </div>
      </form>
    </div>
  )
}

export default RegisterBalance;
