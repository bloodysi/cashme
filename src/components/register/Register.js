import React, { useState, useContext } from 'react';
import { Link, Redirect } from 'react-router-dom';

import { Context } from '../../context';

const Register = () => {

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')

  const { handleRegister, user, error, setError } = useContext(Context)

  if(user){
    return <Redirect to='/' />
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if(username === '' || password === '' || repeatPassword === '' ) {
      return setError('Complete all the fields')
    }
    if(password !== repeatPassword){
      setPassword('')
      setRepeatPassword('')
      return setError('Password do not match')
    }
    handleRegister({username, password})
  }

  return (
    <div className='wrapper middle center'>
       <form onSubmit={(e)=> handleSubmit(e)}>
         <div className='form-title-container'>
          <h2 className='form-title'>SignUp</h2>
          {error ? <span className='error-message red'>{error}</span> : null}
         </div>
          <div className='form-control'>
            <input type='text' value={username} onChange={(e)=> setUsername(e.target.value)}placeholder='Username' />
          </div>
          <div className='form-control'>
            <input type='password' value={password} onChange={(e)=> setPassword(e.target.value)}placeholder='Password' />
          </div>
          <div className='form-control'>
            <input type='password' value={repeatPassword} onChange={(e)=> setRepeatPassword(e.target.value)}placeholder='Repeat Password' />
          </div>               
          <div className='form-button'>
            <button type='submit' className='btn green big'>Register</button>
          </div>
          <span className='redirect'><Link to='/login'>SignIn</Link></span>
        </form>
    </div>
  )
};

export default Register;
