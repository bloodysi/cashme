import React, { useContext } from 'react';
import CountUp from 'react-countup';
import './balance.css';

import { Context } from '../../context';

const Balance = () => {

  const { user } = useContext(Context)

  return (
    <>
      <p className='title-section balance-title'>YOUR BALANCE</p>
      <div className='balance-container'>      
        <h3 className='balance-money'>$<CountUp end={user.balance} separator=',' duration={1.2}/>.00</h3>
      </div>
    </>
  )
}

export default Balance