import React, { useContext } from 'react';
import './logout.css';

import { Context } from '../../context';

const LogOut = ({ setLogoutModal }) => {

  const { handleLogOut } = useContext(Context)

  const handleSubmint = (e) => {
    handleLogOut(e)
    setLogoutModal(false)
  }

  return (
    <div className='logout-container'>
      <form onSubmit={(e)=> handleSubmint(e)} className='logout-form'>
        <h5>Sure to exit</h5>
        <div className='logout-form-buttons'>
          <button type='button' onClick={()=> setLogoutModal(false) }>Cancel</button>
          <button type='submit' className='logout red'><i className="fas fa-power-off"></i></button>
        </div>
      </form>
    </div>
  )
};

export default LogOut;
