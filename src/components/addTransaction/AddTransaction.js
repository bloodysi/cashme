import React, { useState, useContext } from 'react';
import './addTransaction.css';

import { Context } from '../../context';

const AddTransaction = ({ setAddingBalance }) => {

  const [subject, setSubject] = useState('')
  const [money, setMoney] = useState('')
  const [type, setType] = useState('')

  const { handleTransaction, user, error, setError } = useContext(Context)

  const handleSubmit = (e) => {
    e.preventDefault()
    if(subject === '' || money === '' || type === '') {
      setError('Complete all the fields')
      return null
    }
    handleTransaction({subject, money: parseInt(money), type, userId: user._id})
    clear()
    
  }

  const clear = () => {
    setAddingBalance(false)
    setSubject('')
    setMoney('')
    setType('')
  }

  return (
    <>
      <div className='center'>
      <div className='form-title-container'>
        <p className='title-section center'>ADD TRANSACTION</p>
        {error ? <span className='error-message red center'>{error}</span> : null}
      </div>
        <form onSubmit={(e)=> handleSubmit(e)}>
          <div className='form-control'>
            <input type='text' value={subject} onChange={(e)=> setSubject(e.target.value)}placeholder='Subject' />
          </div>
          <div className='form-control'>
            <input type='number' value={money} onChange={(e)=> setMoney(e.target.value)}placeholder='Money' />
          </div>
          <div>
            <button type='button' onClick={()=> setType('Income')} className={type === 'Income' ? 'btn-empty green' : 'btn-empty'} >Income</button>
            <button type='button' onClick={()=> setType('Expense')} className={type === 'Expense' ? 'btn-empty red' : 'btn-empty'} >Expense</button>
          </div>
          <div className='form-button'>
            <button type='button' onClick={clear} className='btn red'>Cancel</button>
            <button type='submit' className='btn green'>Save</button>
          </div>
        </form>
      </div>
    </>
  )
}

export default AddTransaction;
