import React, { useState, useContext } from 'react';
import './navbar.css';

import LogOut from '../logout/LogOut';
import { Context } from '../../context';

const NavBar = () => {

  const [logoutModal, setLogoutModal] = useState(false)

  const { user } = useContext(Context)

  // if(!user){
  //   return setLogoutModal(false)
  // }

  return (
    <header className='header-container'>
      <nav className='navbar'>
        <h2 className='navbar-title'>CashMe</h2>
        {user
          ?
            !logoutModal 
              ?
              <ul className='navbar-link-container'>
                <li className='navbar-link'>{user.username}</li>
                <li onClick={()=> setLogoutModal(true)} className='navbar-link'><i className="fas fa-power-off"></i></li>
              </ul>
              :
              <LogOut setLogoutModal={setLogoutModal}/>
          :
          null
        }
      </nav>      
    </header>
  )
}

export default NavBar;
