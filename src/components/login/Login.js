import React, { useState, useContext } from 'react';
import { Link, Redirect } from 'react-router-dom';

import { Context } from '../../context';


const Login = () => {

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('') 
  
  const { handleLogin, user, error, setError } = useContext(Context)

  if(user){
    return <Redirect to='/' />
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if(username === '' || password === '') {
      return setError('Complete all the fields')
    }    
    handleLogin({username, password})
    setUsername('')
    setPassword('')
  }

  return (
    <div className='wrapper middle center'>
       <form onSubmit={(e)=> handleSubmit(e)}>
         <div className='form-title-container'>
          <h2 className='form-title'>SignIn</h2>
          {error ? <span className='error-message red'>{error}</span> : null}
         </div>
          <div className='form-control'>
            <input type='text' value={username} onChange={(e)=> setUsername(e.target.value)}placeholder='Username' />
          </div>
          <div className='form-control'>
            <input type='password' value={password} onChange={(e)=> setPassword(e.target.value)}placeholder='Password' />
          </div>          
          <div className='form-button'>
            <button type='submit' className='btn green big'>Login</button>
          </div>
          <span className='redirect'><Link to='/register'>SignUp</Link></span>
        </form>
    </div>
  )
}

export default Login;
