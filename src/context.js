import React, { Component, createContext } from 'react';

import cookie from 'js-cookie';
import axios from 'axios';

export const Context = createContext()

export class ContextProvider extends Component {

  state = {
    items: cookie.getJSON('items') || [],
    user: cookie.getJSON('user') || null,
    error: ''
  }

  handleRegister = async (info) => {
    const { data } = await axios('users/register',{
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST',
      data: info
    })
    if(!data.error){
      cookie.set('user', JSON.stringify(data))
      this.setState({user: data})
    }
    if(data.error){
      this.setError(data.body)
    }
  }

  handleLogin = async (info) => {
    const { data } = await axios('users/login',{
      headers:{
        'content-type': 'application/json'
      },
      method: 'POST',
      data: info
    })
    if(!data.error){
      cookie.set('user', JSON.stringify(data))
      this.setState({user: data})
      this.getItems()
    }
    if(data.error){
      this.setError(data.body)
    }
    
  }

  handleLogOut = (e) => {
    e.preventDefault()
    cookie.remove('user')
    cookie.remove('items')
    this.setState({user: null, items: []})
  }

  handleBalance = async (cash) => {
    const { data } = await axios(`action/balance/${this.state.user._id}`, {
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST',
      data: {
        balance: parseInt(cash)
      }
    })
    if(data){
      this.setState({user: data.body})
      cookie.set('user', JSON.stringify(data.body))
    }
  }

  getItems = async () => {
    const { data } = await axios(`action/transaction/${this.state.user._id}`)
    if(data){
      this.setState({items: data.body})
      cookie.set('items', JSON.stringify(data.body))
    }
  }

  getUser = async () => {
    const { data } = await axios(`users/user/${this.state.user._id}`)
    if(data){
      this.setState({user: data.body})
      cookie.set('user', JSON.stringify(data.body))
    }
  }

  handleTransaction = async (info) => {
    const { data } = await axios('action/transaction',{
      headers:{
        'content-type': 'application/json'
      },
      method: 'POST',
      data: info
    })
    if(data){
      this.getItems()
      this.getUser()
    }

  }

  setError = (e) => {
    this.setState({error: e})
    setTimeout(()=> {
      this.setState({error: ''})
    }, 2000)
  }

  numbersComa = (number) => {

    let first = '';
    let second;
    let third;

    if(number.length > 0 && number.length < 4 ){
      first = number.slice(0, number.length)
    }
  }


  render(){
    this.numbersComa('12000')
    return (
      <Context.Provider value={{
        ...this.state,
        setError: this.setError,
        handleLogin: this.handleLogin,
        handleLogOut: this.handleLogOut,
        handleRegister: this.handleRegister,
        handleBalance: this.handleBalance,
        handleTransaction: this.handleTransaction
      }}>
        {this.props.children}
      </Context.Provider>
    )
  }
}